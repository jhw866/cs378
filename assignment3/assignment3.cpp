#include <stdio.h>
#include <stdlib.h>

void easyScalar(float *x, float *y, int size, float alpha) {
	for(int i = 0; i < size; ++i) {
		y[i] = x[2*i] * x[2*i] + x[2*i+1]/alpha;
		printf("y[%i] = %f\n", i, y[i]);
	}
}

void easyIntrinsics(float *x, float *y, int size, float alpha) {

}

void FIRScalar(float *x, float *y, float *h, int size) {
	for(int i = 4; i < size; i++) 
		y[i] = h[3] * x[i-3] + h[2] * x[i-2] + h[1] * x[i-1] + h[0] * x[i];
}

void FIRIntrinsics(float *x, float *y, float *h, int size) {


}

float *createArray(int size) {

	float* array = new float[size];
	for(int i = 0; i < size; i++) {
		array[i] = 2.0;
	}
	return array;
}

int main(int argc, char **argv) {
	
	float *x = createArray(10);
	float *y = createArray(5);

	easyScalar(x, y, 5, 2.0);

	return 0;
}
