#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;


static const int matrixSize = 1000;
static const int b = 50;

int** createMatrix() {
	int** table = new int*[matrixSize];
	for (int i = 0; i < matrixSize; i++)
	{
   		table[i] = new int[matrixSize];
		for(int j = 0; j < matrixSize; j++) {
			table[i][j] = 0;
		}
	}
	return table;
}

int main(int argc, char** argv) {
		
	int** A = createMatrix();
	int** B = createMatrix();
	int** C = createMatrix();

	fprintf(stdout,"Starting\n");	
	for(int kk = 0; kk < matrixSize; kk += b) {
		for(int jj = 0; jj < matrixSize; jj += b) {
			for(int i = 0; i < matrixSize; i++) {
				for(int k = kk; k < min(kk + b - 1, matrixSize); k++) {
					int r = A[i][k];
					for(int j = jj; j < min(jj + b - 1, matrixSize); j++) {
						C[i][j] += r * B[k][j];
					}
				}
			}
		}
	}
	fprintf(stdout, "done\n");
	return 0;
}
