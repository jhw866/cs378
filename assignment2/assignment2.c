#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void clearCache(void) {
	int i;
	int j;
	const int size = 10*1024*1024; // Allocate 20M. Set much larger then L2
	char *c = (char *)malloc(size);
	for (i = 0; i < 500; i++) {
		for(j = 0; j < size; j++)
	        	c[i] = c[i] + c[i];
	}
	free(c);
}

int** createMatrix(int size, int matrixType) {
	int **matrix;
	int i;
	int j;
	int number;

	

	// Allocate memomy for size of i
	matrix = malloc(size * sizeof(int *));
	if(matrix == NULL) {
		fprintf(stderr, "We are out of memory");
		exit(-1);
	}

	for(i = 0; i < size; i++) {
		// Allocate memory for each i
		matrix[i] = malloc(size * sizeof(int *));
		if(matrix[i] == NULL) {
			fprintf(stderr, "We are out of memory");
			exit(-1);
		}

		// Put random number into here
		for(j = 0; j < size; j++) {
			if(matrixType == 3)
				matrix[i][j] = 0;
			else {
				number = rand() % 10;
				matrix[i][j] = number;
			}
		}
	} // EndFor

	return matrix;
}

void testMatrix(int **matrix, int size) {
	int row;
	int column;
	for(row = 0; row < size; row++) {
		printf("[");
		for(column = 0; column < size; column++) {
			printf(" %i ", matrix[row][column]);
		}
		printf("]\n");
	}
	printf("\n");
}

void runTest(void) {
	// Various Variables need for going through problems
	int row;
	int column;
	int k;
	int problemSize;

	// Variables need to hold matrices
	int **matrix1;
	int **matrix2;
	int **matrix3;

	for(problemSize = 5; problemSize <= 100 ; problemSize += 5) {

		// Create matricies
		matrix1 = createMatrix(problemSize, 1);
		matrix2 = createMatrix(problemSize, 2);
		matrix3 = createMatrix(problemSize, 3);
		// Clear Cache method
		clearCache();
		for(row = 0; row < problemSize; row++) {
			
			for(column = 0; column < problemSize; column++) {
				
				for(k = 0; k < problemSize; k++) {
					matrix3[row][column] = matrix3[row][column] + (matrix1[row][k] * matrix2[k][column]); 
				}
			}
		}
		// Begin running tests
		free(matrix1);
		free(matrix2);
		free(matrix3);
	}

}

int main() {
	struct timeval te;
	gettimeofday(&te, NULL);
	long int startSeconds = te.tv_sec;
	long int startMicro = te.tv_usec;
	runTest();
	gettimeofday(&te, NULL);
	long int totalSeconds = te.tv_sec - startSeconds;
	long int totalMicroSeconds = te.tv_usec - startMicro;
	printf("Done in %ld.%ld\n", totalSeconds, totalMicroSeconds);

}
