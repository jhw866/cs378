#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<sys/time.h>

int main() {
	int i;
	struct timespec ts;
	struct timeval te;
	// TODO gettimeofday
	printf("--------------------------------------------------\n");
	printf("gettimeofday timer\n");
	printf("--------------------------------------------------\n");
	for(i = 0; i < 10; i++) {
		gettimeofday(&te, NULL);
		printf("%ld.%ld\n", te.tv_sec, te.tv_usec);
	}
	printf("\n--------------------------------------------------\n");
	// TODO rdtsc


	printf("rdtsc instruction timer\n");
	printf("--------------------------------------------------\n");
	unsigned long long int x;
  	unsigned a, d;
	for(i = 0; i < 10; i++) {
		__asm__ volatile("rdtsc" : "=a" (a), "=d" (d));
   		printf("%llu\n", ((unsigned long long)a) | (((unsigned long long)d) << 32));
	}
	printf("\n--------------------------------------------------\n");

	// TODO get all 4 clocks of clock_gettime
	printf("clock_gettime timer using CLOCK_REALTIME\n");
	printf("--------------------------------------------------\n");
	for(i = 0; i < 10; i++) {
		clock_gettime(CLOCK_REALTIME, &ts);
		printf("%ld.%ld\n", ts.tv_sec, ts.tv_nsec);
	}
	printf("\n--------------------------------------------------\n");

	printf("clock_gettime timer using CLOCK_MONOTONIC\n");
	printf("--------------------------------------------------\n");
	for(i = 0; i < 10; i++) {
		clock_gettime(CLOCK_MONOTONIC, &ts);
		printf("%ld.%ld\n", ts.tv_sec, ts.tv_nsec);
	}
	printf("\n--------------------------------------------------\n");

	printf("clock_gettime timer using CLOCK_PROCESS_CPUTIME_ID\n");
	printf("--------------------------------------------------\n");
	for(i = 0; i < 10; i++) {
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ts);
		printf("%ld.%ld\n", ts.tv_sec, ts.tv_nsec);
	}
	printf("\n--------------------------------------------------\n");

	printf("clock_gettime timer using CLOCK_THREAD_CPUTIME_ID\n");
	printf("--------------------------------------------------\n");
	for(i = 0; i < 10; i++) {
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts);
		printf("%ld.%ld\n", ts.tv_sec, ts.tv_nsec);
	}

}
