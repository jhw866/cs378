#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	struct timespec spec;

	clock_getres(CLOCK_REALTIME, &spec);	
	printf( "Realtime: Seconds: %ld Nanos: %ld\n", spec.tv_sec, spec.tv_nsec );

	clock_getres(CLOCK_MONOTONIC, &spec);
	printf( "CLOCK MONOTONIC Seconds: %ld Nanos: %ld\n", spec.tv_sec, spec.tv_nsec );

	clock_getres(CLOCK_PROCESS_CPUTIME_ID, &spec);
	printf( "CLOCK PROCESS TIME Seconds: %ld Nanos: %ld\n", spec.tv_sec, spec.tv_nsec );

	clock_getres(CLOCK_THREAD_CPUTIME_ID, &spec);
	printf( "CLOCK THREAD CPUTTIME Seconds: %ld Nanos: %ld\n", spec.tv_sec, spec.tv_nsec );
	return 0;
}
