#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "graph.h"



Edge::Edge(int org, int dest, int dist){
    
        origin = org;
        destination = dest;
        distance = dist;
}

int Edge::getOrigin() {
	return origin;
}

int Edge::getDestination() {
	return destination;
}

int Edge::getDistance() {
    return distance;
}


Vertex::Vertex(int id) {		// change to int

    name = id;
    label = -1;
}

void Vertex::addEdge(int v, int distance){

    Edge newEdge(name, v, distance);
    edges.push_back(newEdge);
}

void Vertex::changeLabel(int lab) {
    label = lab;
}

int Vertex::getName() {
	return name;
} 

int Vertex::getLabel() {
    return label;
}

vector<Edge> Vertex::getEdges() {
	return edges;
}

/*
    Creates a linked list like graph of the size of nodes
    Number of edge will always equal the 2 * nodes

    Can probably move this function to the Graph class
*/
struct Graph createLinkedGraph(int nodes) {
    struct Graph g;
    Vertex v = Vertex(0);
    // put first node into graph
    g.graph.push_back(v);
    srand(time(NULL));
    int i = 1;
    for(i = 1; i < nodes; i++) {
            Vertex k = Vertex(i);
            g.graph.push_back(k);
            int num = rand() % 10;
            // add edge from previous node to current node
            g.graph[i-1].addEdge(g.graph[i].getName(), 1);
            // add edge from current node to previous node
            g.graph[i].addEdge(g.graph[i-1].getName(), 1);
    }

    // add edge from last node to the first node
    g.graph[i-1].addEdge(g.graph[0].getName(), 1);
    // add edge from the first node to the las node
    g.graph[0].addEdge(g.graph[i-1].getName(), 1);

    
    // Debug stuff
/*
    for(int i = 0; i < nodes; i++) {
        Vertex v = g.graph[i];
        vector<Edge>::iterator it;
        // get edges
        vector<Edge> edges = v.getEdges();
        it = edges.begin();
        while(it != edges.end()) {
            int num = it->getDestination();
            int num2 = g.graph[i].getName();
            int dist = it->getDistance();
            printf("Node %i has edge to %i with distance %i\n", num2, num, dist);
            it++;
        }
    }
  */  
    g.edges = 2 * nodes;
    g.nodes = nodes;

    return g;
}

/* 
    Creates a dense graph, or one that is half connected

    Number of edges varies

    Can probably move this function to the Graph class

*/
struct Graph createDenseGraph(int nodes) {
    struct Graph dg;
    // Create Graph with given number of nodes
    for(int i = 0; i < nodes; i++) {
        Vertex v = Vertex(i);
        dg.graph.push_back(v);
    }

    // Give 50/50 chance to add edge to each node in the graph
    dg.edges = 0;
    dg.nodes = nodes;
    srand(time(0));
    // Size of vertex is going to be the size of nodes anyway
    for(int i = 0; i < nodes; i++) {
        for(int j = 0 ; j < nodes; j++) {
            if(i != j) {
                int ran = rand() % 2;
                // if 0, add edge; else don't do anything
                if(ran == 0) {
                    int num = (rand() % 10) + 1;
                    dg.graph[i].addEdge(dg.graph[j].getName(), num);
                    dg.edges++;
                }
            }
        }
    }

    
    // Debug stuff
	
    for(int i = 0; i < nodes; i++) {
        Vertex v = dg.graph[i];
        vector<Edge>::iterator it;
        // get edges
        vector<Edge> edges = v.getEdges();
        it = edges.begin();
        while(it != edges.end()) {
            int num = it->getDestination();
            int num2 = dg.graph[i].getName();
            int dist = it->getDistance();
            printf("Node %i has edge to %i with distance %i\n", num2, num, dist);
            it++;
        }
    }  
	
    return dg;
}

struct Graph graphFromFile(FILE* file) {
    char a[10];
    char b[10];
    int nodes;
    int edges;

    fscanf(file, "%s" "%s" "%d" "%d", a, b, &nodes, &edges);
    struct Graph g;
    g.nodes = nodes;
    g.edges = edges;
    for(int i = 0; i < nodes; i++) {
        Vertex v = Vertex(i);
        g.graph.push_back(v);
    }

    int origin, destination, distance;
    int i = 0;
    while(fscanf(file, "%s" "%d" "%d" "%d", a, &origin, &destination, &distance) == 4 ) {
        //printf("origin = %i ---- destination = %i ---- distance = %i\n", origin, destination, distance);
        g.graph[origin-1].addEdge(g.graph[destination-1].getName(), distance);
        i++;
    }
    
    return g;
}

struct Graph tempGraph(void) {
    struct Graph g;
    for(int i = 0; i < 8; i++) {
        Vertex v = Vertex(i);
        g.graph.push_back(v);
    }

    // All incident to 0
    g.graph[0].addEdge(g.graph[1].getName(), 2);
    g.graph[1].addEdge(g.graph[0].getName(), 2);

    g.graph[0].addEdge(g.graph[2].getName(), 5);
    g.graph[2].addEdge(g.graph[0].getName(), 5);

    g.graph[0].addEdge(g.graph[3].getName(), 4);
    g.graph[3].addEdge(g.graph[0].getName(), 4);

    // All incident to 1
    g.graph[1].addEdge(g.graph[4].getName(), 12);
    g.graph[4].addEdge(g.graph[1].getName(), 12);

    g.graph[1].addEdge(g.graph[2].getName(), 2);
    g.graph[2].addEdge(g.graph[1].getName(), 2);

    g.graph[1].addEdge(g.graph[5].getName(), 7);
    g.graph[5].addEdge(g.graph[1].getName(), 7);

    // All incident to 2
    g.graph[2].addEdge(g.graph[5].getName(), 4);
    g.graph[5].addEdge(g.graph[2].getName(), 4);

    g.graph[2].addEdge(g.graph[3].getName(), 1);
    g.graph[3].addEdge(g.graph[2].getName(), 1);

    g.graph[2].addEdge(g.graph[6].getName(), 3);
    g.graph[6].addEdge(g.graph[2].getName(), 3);

    // All incident to 3
    g.graph[3].addEdge(g.graph[6].getName(), 4);
    g.graph[6].addEdge(g.graph[3].getName(), 4);

    // All incident to 4
    g.graph[4].addEdge(g.graph[7].getName(), 3);
    g.graph[7].addEdge(g.graph[4].getName(), 3);

    // All incident to 5
    g.graph[5].addEdge(g.graph[6].getName(), 1);
    g.graph[6].addEdge(g.graph[5].getName(), 1);

    g.graph[5].addEdge(g.graph[7].getName(), 5);
    g.graph[7].addEdge(g.graph[5].getName(), 5);

    // All incident to 6
    g.graph[6].addEdge(g.graph[7].getName(), 7);
    g.graph[7].addEdge(g.graph[6].getName(), 7);

    g.edges = 28;
    g.nodes = 8;
    return g;
}