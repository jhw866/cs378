#ifndef DIKJSTRA_H
#define DIKJSTRA_H

void setToInfinity(void);

int fifo_pop(void);

void do_fifo(void);

void dikjstra_fifo(struct CRS crs, int thread_num);

int lifo_pop(void);

void do_lifo(void);

void dikjstra_lifo(struct CRS crs, int thread_num);

int priority_pop(void);

void do_priority(void);

void dikjstra_priority(struct CRS crs, int thread_num);

int stealing_pop(int thread_num);

void do_stealing(int thread_num);

void dikjstra_stealing(struct CRS crs, int thread_num);
#endif
