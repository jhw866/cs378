#include <queue>
#include <stack>
#include <stdio.h>
#include <time.h>
#include <mutex>
#include <atomic>
#include <thread>
#include <vector>
#include <limits.h>
#include "crs.h"

using namespace std;
struct CRS temp;
struct lessThanAge{
	bool operator()(int left, int right) {
		return temp.orig[left].distance > temp.orig[right].distance;
	}
};

struct workstealing {
	priority_queue<int, vector<int>, lessThanAge > queue;
	mutex mut;
};

struct workstealing *work;


queue<int> fifo;
stack<int> lifo;
priority_queue<int, vector<int>, lessThanAge > pq;
mutex fifo_lock;
mutex lifo_lock;
mutex pq_lock;
int t;
int size;

atomic<int> pushes;
atomic<int> iterations;

/*
	Simple method to set all nodes in the CRS to infinity
	Used for all methods
*/

void setToInfinity(void) {

	for(int i = 0; i < temp.nodes; i++)
		temp.orig[i].distance = INT_MAX;
}



/*
	Method that pops the value from the queue. This does it synchronously
	to make sure that there are no misreads or writes to the queue.
*/
int fifo_pop(void) {
	
	int ret = -1;
	while(pushes != iterations && ret == -1) {
		fifo_lock.lock();		// Lock
		if(fifo.empty()) 		// If the queue is empty there are no values
			ret = -1;
		else {
			ret = fifo.front();	// Read the value
			fifo.pop();			// Pop the value
			printf("access = %i\n", ret);
			int sizefifo = fifo.size();
			queue<int> temp;
			printf("queue after pull = ");
			for(int i = 0; i < sizefifo; i++) {
				int tempnum = fifo.front();
				fifo.pop();
				printf("%i ", tempnum);
				temp.push(tempnum);
			}
			for(int i = 0 ; i < sizefifo; i++) {
				int tempnum = temp.front();
				temp.pop();
				fifo.push(tempnum);
			}
			printf("\n");
		}
		fifo_lock.unlock();		// Release the lock
	}
	return ret;
}

/*
	This method does the actual Dijkstra's algorithm for a fifo scheduler.
	This method should be used to do threads.
*/
void do_fifo(void) {

	// Origin value
	int orig; 
//	printf("thread created\n");
	// Synchronously check queue to see if there is a value
	while((orig=fifo_pop()) != -1) {
	
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			// lock origin to make sure getting right value
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			// unlock origin
			int dest = temp.dest[i];
			// if alt is less than the current distance
			// lock destination node
			temp.orig[dest].mut.lock();
			if(alt < temp.orig[dest].distance) {
				pushes++;
				temp.orig[dest].distance = alt;
				fifo_lock.lock();
				fifo.push(dest);
				printf("pushing = %i\n", dest);
				int sizefifo = fifo.size();
				queue<int> temp;
				printf("queue after pushing = ");
				for(int i = 0; i < sizefifo; i++) {
					int tempnum = fifo.front();
					fifo.pop();
					printf("%i ", tempnum);
					temp.push(tempnum);
				}
				for(int i = 0 ; i < sizefifo; i++) {
					int tempnum = temp.front();
					temp.pop();
					fifo.push(tempnum);
				}
				printf("\n");
				fifo_lock.unlock();
			}
			// unlock destination mode
			temp.orig[dest].mut.unlock();
			if(t < 5)
				t++;
			
		}
		iterations++;
	}
//	printf("thread exiting\n");

}


/* 
	Fifo queue method for Dikjstra's algorithm. Does prepatory work
	for the begin the threads
*/
void dikjstra_fifo(struct CRS crs, int thread_num) {
	printf("starting\n");
	// Set the crs to be seen by the entire program
	temp = crs;
	// Set all nodes to infinity
	setToInfinity();
	t = 0;
	pushes = 1;
	iterations = 0;
	temp.orig[0].distance = 0;

	fifo.push(0);

	vector<thread> threads;

	struct timespec begin, end;
	clock_gettime(CLOCK_MONOTONIC, &begin);
	threads.emplace_back(do_fifo);
	
	//while(t<5);
	
	for(int i = 1; i < thread_num; i++)
		threads.emplace_back(do_fifo);


	for(auto& th : threads)
		th.join();

	clock_gettime(CLOCK_MONOTONIC, &end);
	double finish = (end.tv_sec - begin.tv_sec) + (end.tv_nsec - begin.tv_nsec) / 1E9;
	printf("\ndone in %f\n\n", finish);
	for(int orig = 0; orig < temp.nodes; orig++) {
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			// lock origin to make sure getting right value
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			// unlock origin
			int dest = temp.dest[i];
			// if alt is less than the current distance
			// lock destination node
			if(temp.orig[dest].distance > alt)
				printf("Hello\n");
			// unlock destination mode
		}
	}


	for(int i = 0; i < temp.nodes; i++) {
        int dist = temp.orig[i].distance;
        printf("Node %i has distance = %i\n", i, dist);
    }


}

/*********************************************************************/
/********************** LIFO *****************************************/
/*********************************************************************/

/*
	Method that pops the value from the queue. This does it synchronously
	to make sure that there are no misreads or writes to the queue.
*/
int lifo_pop(void) {
	
	int ret = -1;
	while(pushes != iterations && ret == -1) {
		lifo_lock.lock();		// Lock
		if(lifo.empty()) 		// If the queue is empty there are no values
			ret = -1;
		else {
			ret = lifo.top();	// Read the value
			lifo.pop();			// Pop the value
			printf("access = %i\n", ret);
			int sizefifo = lifo.size();
			stack<int> temp;
			printf("stack after pull = ");
			for(int i = 0; i < sizefifo; i++) {
				int tempnum = lifo.top();
				lifo.pop();
				printf("%i ", tempnum);
				temp.push(tempnum);
			}
			for(int i = 0 ; i < sizefifo; i++) {
				int tempnum = temp.top();
				temp.pop();
				lifo.push(tempnum);
			}
			printf("\n");
		}
		lifo_lock.unlock();		// Release the lock
	}
	return ret;
}

/*
	This method does the actual Dijkstra's algorithm for a lifo scheduler.
	This method should be used to do threads.
*/
void do_lifo(void) {

	// Origin value
	int orig; 
//	printf("thread created\n");
	// Synchronously check queue to see if there is a value
	while((orig=lifo_pop()) != -1) {
	
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			// lock origin to make sure getting right value
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			// unlock origin
			int dest = temp.dest[i];
			// if alt is less than the current distance
			// lock destination node
			//printf("orig = %i\n", orig);
			temp.orig[dest].mut.lock();
			if(alt < temp.orig[dest].distance) {
				pushes++;
				temp.orig[dest].distance = alt;
				lifo_lock.lock();
				lifo.push(dest);
				printf("Pushing = %i\n", dest);
				int sizefifo = lifo.size();
				stack<int> temp;
				printf("stack after push = ");
				for(int i = 0; i < sizefifo; i++) {
					int tempnum = lifo.top();
					lifo.pop();
					printf("%i ", tempnum);
					temp.push(tempnum);
				}
				for(int i = 0 ; i < sizefifo; i++) {
					int tempnum = temp.top();
					temp.pop();
					lifo.push(tempnum);
				}
				printf("\n");
				lifo_lock.unlock();
			}
			// unlock destination mode
			temp.orig[dest].mut.unlock();
			if(t < 5)
				t++;
			
		}
		iterations++;
	}
//	printf("thread exiting\n");

}


/* 
	Lifo queue method for Dikjstra's algorithm. Does prepatory work
	for the begin the threads
*/
void dikjstra_lifo(struct CRS crs, int thread_num) {
	printf("starting\n");
	// Set the crs to be seen by the entire program
	temp = crs;
	// Set all nodes to infinity
	setToInfinity();
	t = 0;
	pushes = 1;
	iterations = 0;
	temp.orig[0].distance = 0;

	lifo.push(0);

	vector<thread> threads;

	struct timespec begin, end;
	clock_gettime(CLOCK_MONOTONIC, &begin);
	threads.emplace_back(do_lifo);
	
	//while(t<5);
	
	for(int i = 1; i < thread_num; i++)
		threads.emplace_back(do_lifo);


	for(auto& th : threads)
		th.join();

	clock_gettime(CLOCK_MONOTONIC, &end);
	double finish = (end.tv_sec - begin.tv_sec) + (end.tv_nsec - begin.tv_nsec) / 1E9;
	printf("\ndone in %f\n\n", finish);
	for(int orig = 0; orig < temp.nodes; orig++) {
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			// lock origin to make sure getting right value
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			// unlock origin
			int dest = temp.dest[i];
			// if alt is less than the current distance
			// lock destination node
			if(temp.orig[dest].distance > alt)
				printf("Hello\n");
			// unlock destination mode
		}
	}

	
	for(int i = 0; i < temp.nodes; i++) {
        int dist = temp.orig[i].distance;
        printf("Node %i has distance = %i\n", i, dist);
    }
	
}

/*********************************************************************/
/********************** Priority Queue *******************************/
/*********************************************************************/
/*
	Method that pops the value from the queue. This does it synchronously
	to make sure that there are no misreads or writes to the queue.
*/
int priority_pop(void) {
	pq_lock.lock();		// Lock
	int ret;
	if(pq.empty()) 		// If the queue is empty there are no values
		ret = -1;
	else {
		ret = pq.top();	// Read the value
		pq.pop();			// Pop the value
		printf("access = %i\n", ret);
		int sizefifo = pq.size();
		priority_queue<int, vector<int>, greater<int> > temp;
		printf("priority queue after pull = ");
		for(int i = 0; i < sizefifo; i++) {
			int tempnum = pq.top();
			pq.pop();
			printf("%i ", tempnum);
			temp.push(tempnum);
		}
		for(int i = 0 ; i < sizefifo; i++) {
			int tempnum = temp.top();
			temp.pop();
			pq.push(tempnum);
		}
		printf("\n");
	}
	pq_lock.unlock();		// Release the lock
	return ret;
}

/*
	This method does the actual Dijkstra's algorithm for a fifo scheduler.
	This method should be used to do threads.
*/
void do_priority(void) {

	// Origin value
	int orig; 
	//printf("thread created\n");
	// Synchronously check queue to see if there is a value
	while((orig=priority_pop()) != -1) {
	
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			// lock origin to make sure getting right value
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			// unlock origin
			int dest = temp.dest[i];
			// if alt is less than the current distance
			// lock destination node
			temp.orig[dest].mut.lock();
			//printf("orig = %i\n", orig);
			if(alt < temp.orig[dest].distance) {
				temp.orig[dest].distance = alt;
				pq_lock.lock();
				pq.push(dest);
				printf("pushing = %i\n", dest);
				int sizefifo = pq.size();
				priority_queue<int, vector<int>, greater<int> > temp;
				printf("priority queue after push = ");
				for(int i = 0; i < sizefifo; i++) {
					int tempnum = pq.top();
					pq.pop();
					printf("%i ", tempnum);
					temp.push(tempnum);
				}
				for(int i = 0 ; i < sizefifo; i++) {
					int tempnum = temp.top();
					temp.pop();
					pq.push(tempnum);
				}
				printf("\n");
				pq_lock.unlock();
			}
			// unlock destination mode
			temp.orig[dest].mut.unlock();
			if(t < 5)
				t++;
			
		}
	}
	//printf("thread exiting\n");

}


/* 
	Fifo queue method for Dikjstra's algorithm. Does prepatory work
	for the begin the threads
*/
void dikjstra_priority(struct CRS crs, int thread_num) {
	printf("starting\n");
	// Set the crs to be seen by the entire program
	temp = crs;
	// Set all nodes to infinity
	setToInfinity();
	t = 0;
	temp.orig[0].distance = 0;

	pq.push(0);

	vector<thread> threads;

	struct timespec begin, end;
	clock_gettime(CLOCK_MONOTONIC, &begin);
	threads.emplace_back(do_priority);
	
	while(t<5);
	
	for(int i = 1; i < thread_num; i++)
		threads.emplace_back(do_priority);


	for(auto& th : threads)
		th.join();

	clock_gettime(CLOCK_MONOTONIC, &end);
	double finish = (end.tv_sec - begin.tv_sec) + (end.tv_nsec - begin.tv_nsec) / 1E9;
	printf("\ndone in %f\n\n", finish);
	for(int orig = 0; orig < temp.nodes; orig++) {
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			// lock origin to make sure getting right value
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			// unlock origin
			int dest = temp.dest[i];
			// if alt is less than the current distance
			// lock destination node
			if(temp.orig[dest].distance > alt)
				printf("Hello\n");
			// unlock destination mode
		}
	}


	for(int i = 0; i < temp.nodes; i++) {
        int dist = temp.orig[i].distance;
        printf("Node %i has distance = %i\n", i, dist);
    }


}

/*
	Method that pops the value from the queue. This does it synchronously
	to make sure that there are no misreads or writes to the queue.
*/
int stealing_pop(int thread_num) {
	int ret;
	work[thread_num].mut.lock();
	
	if(work[thread_num].queue.empty()) {
		work[thread_num].mut.unlock();
		bool found = false;
		while(pushes != iterations && found == false) {
			//printf("therad %i looking for work\n", thread_num);
			for(int i = 0; i < size; i++) {
				work[i].mut.lock();
				if(!work[i].queue.empty()) {
					ret = work[i].queue.top();
					work[i].queue.pop();
					found = true;
				}
				work[i].mut.unlock();
				if(found == true)
					return ret;
			}
		}
	}
	else {
		ret = work[thread_num].queue.top();
		//printf("thread %i poping\n", thread_num);
		work[thread_num].queue.pop();
		printf("access = %i\n", ret);
		int sizefifo = work[thread_num].queue.size();
		priority_queue<int, vector<int>, greater<int> > temp;
		printf("priority queue after pull = ");
		for(int i = 0; i < sizefifo; i++) {
			int tempnum = work[thread_num].queue.top();
			work[thread_num].queue.pop();
			printf("%i ", tempnum);
			temp.push(tempnum);
		}
		for(int i = 0 ; i < sizefifo; i++) {
			int tempnum = temp.top();
			temp.pop();
			work[thread_num].queue.push(tempnum);
		}
		printf("\n");
		work[thread_num].mut.unlock();
		return ret;
	}

	return -1;
}

void do_stealing(int thread_num) {

	// Origin value
	int orig; 
	//printf("thread created\n");
	// Synchronously check queue to see if there is a value
	while((orig=stealing_pop(thread_num)) != -1) {
	
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			//printf("thread %i doing work\n", thread_num);
			// lock origin to make sure getting right value
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			// unlock origin
			int dest = temp.dest[i];
			// if alt is less than the current distance
			// lock destination node
			temp.orig[dest].mut.lock();
			if(alt < temp.orig[dest].distance) {
				pushes++;
				temp.orig[dest].distance = alt;
				work[thread_num].mut.lock();
				work[thread_num].queue.push(dest);
				printf("pushing = %i\n", dest);
				int sizefifo = work[thread_num].queue.size();
				priority_queue<int, vector<int>, greater<int> > temp;
				printf("priority queue after push = ");
				for(int i = 0; i < sizefifo; i++) {
					int tempnum = work[thread_num].queue.top();
					work[thread_num].queue.pop();
					printf("%i ", tempnum);
					temp.push(tempnum);
				}
				for(int i = 0 ; i < sizefifo; i++) {
					int tempnum = temp.top();
					temp.pop();
					work[thread_num].queue.push(tempnum);
				}
				printf("\n");
				work[thread_num].mut.unlock();
			}
			// unlock destination mode
			temp.orig[dest].mut.unlock();
			if(t < 5)
				t++;
			
		}
		iterations++;
	}
	//printf("thread exiting\n");

}

void dikjstra_stealing(struct CRS crs, int thread_num) {

	printf("starting\n");
	// Set the crs to be seen by the entire program
	temp = crs;
	// Set all nodes to infinity
	setToInfinity();
	t = 0;
	size = thread_num;
	pushes = 1;
	iterations = 0;

	work = new workstealing[thread_num]();
	temp.orig[0].distance = 0;

	work[0].queue.push(0);

	vector<thread> threads;

	struct timespec begin, end;
	clock_gettime(CLOCK_MONOTONIC, &begin);
	//threads.emplace_back(do_stealing, 0);
	
	for(int i = 0; i < thread_num; i++)
		threads.emplace_back(do_stealing, i);


	for(auto& th : threads)
		th.join();

	clock_gettime(CLOCK_MONOTONIC, &end);
	double finish = (end.tv_sec - begin.tv_sec) + (end.tv_nsec - begin.tv_nsec) / 1E9;
	printf("done in %f\n\n", finish);
	
	for(int orig = 0; orig < temp.nodes; orig++) {
		// Get the ending index value for origin
		int end = temp.index[orig];		
		int begin;

		// If origin is 0 in the CRS, set beginning to 0
		if(orig == 0)			
			begin = 0;
		// Else set it to the ending index of the value before origin
		else					
			begin = temp.index[orig - 1];

		// Go through all the nodes that are incident to origin
		for(int i = begin; i < end; i++) {
			
			int alt = temp.orig[orig].distance + temp.edge_data[i];
			
			int dest = temp.dest[i];
			//printf("orig dist = %i ---- dest dist = %i ----- alt = %i\n", temp.orig[orig].distance, temp.orig[dest].distance, alt);
			// if alt is less than the current distance
			if(temp.orig[dest].distance > alt)
				printf("Hello\n");
		}
	}


	
	for(int i = 0; i < temp.nodes; i++) {
	        int dist = temp.orig[i].distance;
	        printf("Node %i has distance = %i\n", i, dist);
	}
	


	

}


