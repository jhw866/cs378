#include <vector>
#include <stdio.h>
#include "crs.h"

struct CRS createCRS(int edges, int n) {
     struct CRS crs;
     crs.orig = new node[n];
     crs.index = new int[n];
     crs.dest = new int[edges];
     crs.edge_data = new int[edges];
     return crs;
}
 
/*
    Converts a given graph g, to a CRS data structure.
    It then returns the CRS data structure
*/
struct CRS convertToCRS(struct Graph g) {
     struct CRS crs;
     crs.orig = new node[g.nodes]();
     crs.index = new int[g.nodes]();
     crs.dest = new int[g.edges]();
     crs.edge_data = new int[g.edges]();
     int counter = 0;
     int nodes = g.nodes;
     for(int i = 0; i < nodes; i++) {
         Vertex v = g.graph[i];
         crs.orig[i].distance = 0;
 
         vector<Edge>::iterator it;
         // get edges
         vector<Edge> edges = v.getEdges();
         it = edges.begin();
         while(it != edges.end()) {
             int dest = it->getDestination();
             int dist = it->getDistance();
             crs.dest[counter] = dest;
             crs.edge_data[counter] = dist;
             counter++;
             it++;
         }
         crs.index[i] = counter;
     }
     crs.nodes = nodes;
	 crs.edges = g.edges;
     return crs;
}

void printCRS(struct CRS crs) {
	
	printf("[");
	for(int i = 0; i < crs.nodes; i++) {
		printf(" %i ", i);
	}
	printf("]\n");

	printf("[");
	for(int i = 0; i < crs.nodes; i++)
		printf(" %i ", crs.index[i]);
	printf("]\n");

	printf("[");
	for(int i = 0; i < crs.edges; i++)
		printf(" %i ", crs.dest[i]);
	printf("]\n");

	printf("[");
	for(int i = 0; i < crs.edges; i++)
		printf(" %i ", crs.edge_data[i]);
	printf("]\n");
}

