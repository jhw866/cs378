#include <stdio.h>
#include <stdlib.h>
#include <queue>
#include <limits.h>
#include "graph.h"
#include "crs.h"
#include "dikjstra.h"

using namespace std;
/*
    Main driver
*/

int thread_num = 1;

int main(){

     struct Graph g = tempGraph();
     struct CRS crs;
 //    crs = convertToCRS(g);
	// g.graph.clear();
	//printCRS(crs);
	//dikjstra_fifo(crs);

	//dikjstra_lifo(crs);

	//dikjstra_priority(crs);

	//dikjstra_stealing(crs, 4);

    FILE *file = fopen("USA-road-t.NY.gr", "r");
   // struct Graph g = graphFromFile(file);
    crs = convertToCRS(g);
    g.graph.clear();

    printf("-------------------Fifo------------------\n");
    dikjstra_fifo(crs, thread_num);
    
    printf("-------------------LIFO------------------\n");
    dikjstra_lifo(crs, thread_num);
    printf("-------------Priority Queue--------------\n");
    dikjstra_priority(crs, thread_num);
    printf("---------------Work stealing-------------\n");
    dikjstra_stealing(crs , thread_num);
	free(crs.orig);
	free(crs.index);
	free(crs.dest);
	free(crs.edge_data);
    
    return 0;
}
