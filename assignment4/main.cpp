#include "graph.h"
#include <stdio.h>
#include <stdlib.h>
#include <queue>

/*
    CRS data structrue. Has place to put in origin array,
    index array, destination array, and edge data array.

    Allows for dynamic allocation
*/
struct CRS {
    /* data */
    int *orig;
    int *index;
    int *dest;
    int *edge_data;
    int nodes;
};

/*
    Adjacency Matrix Data structure. Simple array and nodes int
    Allows for the abililty to allocate data on the heap
*/
struct adjMatrix {
    /* data */
    int *array;
    int nodes;
};



/*
    Creates a CRS struct. Haven't used this function yet, though it probably 
    will come in handy.
*/
struct CRS createCRS(int edges, int n) {
    struct CRS crs;
    crs.orig = new int[n];
    crs.index = new int[n];
    crs.dest = new int[edges];
    crs.edge_data = new int[edges];
    return crs;
}




/*
    Converts a given graph g, to a CRS data structure.
    It then returns the CRS data structure
*/
struct CRS convertToCRS(int nodes, struct Graph g) {
    struct CRS crs;
    crs.orig = new int[nodes];
    crs.index = new int[nodes];
    crs.dest = new int[g.edges];
    int counter = 0;
    for(int i = 0; i < nodes; i++) {
        Vertex v = g.graph[i];
        int orig = v.getName();
        crs.orig[i] = orig;

        vector<Edge>::iterator it;
        // get edges
        vector<Edge> edges = v.getEdges();
        it = edges.begin();
        while(it != edges.end()) {
            int dest = it->getDestination();
            crs.dest[counter] = dest;
            counter++;
            it++;
        }
        crs.index[i] = counter;
    }
    crs.nodes = nodes;
    return crs;
}

/*
    Converts a given graph g, to a Adjacency Matrix data structure.
    It then returns the Adjacency Matrix data structure
*/
struct adjMatrix convertToAdjMatrix(int nodes, struct Graph g) {
    struct adjMatrix matrix;
    matrix.array = new int[nodes*nodes]();
    for(int i = 0; i < nodes; i++) {
        Vertex v = g.graph[i];
        vector<Edge>::iterator it;
        // get edges
        vector<Edge> edges = v.getEdges();
        it = edges.begin();

        while(it != edges.end()) {
            int dest = it->getDestination();
            matrix.array[i*nodes + dest] = 1;
            it++;
        }
    }
    matrix.nodes = nodes;
    return matrix;
}

/*
    Runs BFS on an adjacency Matrix. Still probably a lot more to do
*/
void adjMatrixBFS(int nodes, struct Graph g, struct adjMatrix matrix) {

    if(matrix.nodes != nodes) {
        fprintf(stderr, "Nodes do not match: Matrix Nodes = %i nodes input = %i\n", matrix.nodes, nodes);
        exit(-1);
    }

    for(int i = 0; i < nodes; i++) {
        g.graph[i].changeLabel(-1);
    }

    g.graph[0].changeLabel(0);
    queue<int> q;
    q.push(g.graph[0].getName()); 
    while(!q.empty()) {
        int index = q.front();
        q.pop();
        for(int i = 0; i < nodes; i++) {
            int edge = matrix.array[index*nodes + i];
            // if there is an edge
            if(edge == 1) {
                // check if node in graph has a label of -1
                if(g.graph[i].getLabel() == -1) {
                    g.graph[i].changeLabel(g.graph[index].getLabel() + 1);
                    q.push(i);
                }
            }
        }

    }

    
    for(int i = 0; i < nodes; i++) {
        printf("node %i  has label %i\n", i, g.graph[i].getLabel());
    }
    
}

/*
    Runs BFS on CRS data structure. Obviously a lot more to do.
*/
void crsBFS(struct CRS crs, struct Graph g) {
    if(crs.nodes != g.nodes) {
        fprintf(stderr, "CRS nodes does not equal graph nodes.\n");
        fprintf(stderr, "CRS nodes = %i : Graph nodes = %i\n", crs.nodes, g.nodes);
        fprintf(stderr, "Exiting\n");
        exit(-1);
    }

    // Set all nodes label to -1
    for(int i = 0; i < g.nodes; i++) {
        g.graph[i].changeLabel(-1);
    }

    // Enqueue the first node
    g.graph[0].changeLabel(0);
    queue<int> q;
    q.push(g.graph[0].getName());

    // Begin BFS
    while(!q.empty()) {
        int index = q.front();
        q.pop();
        int end = crs.index[index];
        int begin;
        if (index == 0)
            begin = 0;
        else
            begin = crs.index[index - 1];
        for(int i = begin; i < end; i++) {
            int node = crs.dest[i];
            if(g.graph[node].getLabel() == -1) {
                g.graph[node].changeLabel(g.graph[index].getLabel() + 1);
                q.push(node);
            }
        }
    }

/*    for(int i = 0; i < g.nodes; i++) {
        printf("node %i  has label %i\n", i, g.graph[i].getLabel());
    }*/
}

/*
    Does BFS for the linked list graph. It runs both the adjacency matrix BFS
    and the CRS BFs
*/
void linkedBFS(int nodes) {
    // Create graph
    struct Graph graph;
    graph = createLinkedGraph(nodes);

    // create adjMatrix for graph
    struct adjMatrix matrix;
    matrix = convertToAdjMatrix(nodes, graph);

    //do BFS for adjMatrix
    adjMatrixBFS(nodes, graph, matrix);
    // Free BFS data structure
    free(matrix.array);

    // create CRS for graph
    struct CRS crs;
    crs = convertToCRS(nodes, graph);

    // do BFS for CRS
    crsBFS(crs, graph);
    // Free CRS data structure
    free(crs.orig);
    free(crs.index);
    free(crs.dest);

}

/*
    Does BFS for the dense graph. It runs both the adjacency matrix BFS
    and the CRS BFs
*/
void denseBFS(int nodes) {

    // Create graph
    struct Graph graph;
    graph = createDenseGraph(nodes);

    // create adjMatrix for graph
    struct adjMatrix matrix;
    matrix = convertToAdjMatrix(nodes, graph);

    //do BFS for adjMatrix

    // Free BFS data structure
    free(matrix.array);

    // create CRS for graph
    struct CRS crs;
    crs = convertToCRS(nodes, graph);

    // do BFS for CRS
    crsBFS(crs, graph);
    // Free CRS data structure
    free(crs.orig);
    free(crs.index);
    free(crs.dest);
}

/*
    Part 1 of the homework. Creates a graph with 3 nodes. The edges are as follows:

    * 0->1
    * 1->2
    * 2->0

    It then creates a CRS and an adjacency matrix based off this graph
*/
void part1() {
    // Creating a minimal graph
    struct Graph g;
    Vertex v  = Vertex(0);
    g.graph.push_back(v);
    v = Vertex(1);
    g.graph.push_back(v);
    v= Vertex(2);
    g.graph.push_back(v);
    g.graph[0].addEdge(g.graph[1].getName());
    g.graph[1].addEdge(g.graph[2].getName());
    g.graph[2].addEdge(g.graph[0].getName());

    g.edges = 3;
    // Do Adjacency matrix
    
    struct adjMatrix matrix;
    matrix = convertToAdjMatrix(3, g);
    // Adjacency matrix debugging
    for(int i = 0; i < 3; i++) {
        printf("[");
        for(int j = 0; j < 3; j++)
            printf(" %i ", matrix.array[i*3 + j]);
        printf("]\n");
    }

    adjMatrixBFS(3, g, matrix);
    // Do CRS part now
    struct CRS crs;
    crs = convertToCRS(3, g);
    // printf("[");
    // for(int i = 0 ; i < 3; i++) {
    //     printf("%i", crs.orig[i]);
    // }
    // printf("]\n");

    // printf("[");
    // for(int i = 0 ; i < 3; i++) {
    //     printf("%i", crs.index[i]);
    // }
    // printf("]\n");

    // printf("[");
    // for(int i = 0 ; i < 3; i++) {
    //     printf("%i", crs.dest[i]);
    // }
    // printf("]\n");
    
    free(matrix.array);
    free(crs.orig);
    free(crs.index);
    free(crs.dest);
}

/*
    Main driver
*/
int main(){

    part1();
    //createLinkedGraph(100);
    //createDenseGraph(10);
    //linkedBFS();
    struct Graph graph;
    int nodes = 120;
    graph = createLinkedGraph(nodes);

    // create adjMatrix for graph
    struct adjMatrix matrix;
    matrix = convertToAdjMatrix(nodes, graph);
    printf("Starting BFS");
    adjMatrixBFS(nodes, graph, matrix);
    printf("Ending BFS");

   /*
    printf("[");
    for(int i = 0 ; i < nodes; i++) {
        printf("%i ", crs.orig[i]);
    }
    printf("]\n");

    printf("[");
    for(int i = 0 ; i < nodes; i++) {
        printf("%i ", crs.index[i]);
    }
    printf("]\n");

    printf("[");
    for(int i = 0 ; i < graph.edges; i++) {
        printf("%i ", crs.dest[i]);
    }
    printf("]\n");
    */

    return 0;
}
